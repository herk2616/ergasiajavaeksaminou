public class EconomySeat extends Seat{

   private int id;
   private int row;
   private int column;
   private Ticket ticket;


   /*
    * Constractor
    */


   public EconomySeat(int id, int row, int column, Ticket ticket){

      super(id, row, column, ticket);
      setId(id);
      setRow(row);
      setColumn(column);
      setTicket(ticket);

   }   // End of constractor {remember.this}


   /*
    * toString
    */


   public String toString(){
      return "Name : "+ticket.getName()+", Row : "+this.row +", Column : "+this.column;
   }   // toString


   /*
    * Setters
    */


   public void setId(int id){
      this.id = id;
   }   // id Setter

   public void setRow(int row){
      this.row = row;
   }   // row Setter

   public void setColumn(int column){
      this.column = column;
   }   // column Setter

   public void setTicket(Ticket ticket){
      this.ticket = ticket;
   }   // ticket Setter


   /*
    * Getters
    */


   public int getId(){
      return this.id;
   }   // id Getter

   public int getRow(){
      return this.row;
   }   // row Getter

   public int getColumn(){
      return this.column;
   }   // column Getter

   public Ticket getTicket(){
      return this.ticket;
   }   // ticket Getter


}   // End of class
