import java.time.LocalDate;
import java.time.LocalTime;

public class Flight{

   private int id;
   private LocalDate departureDate;
   private LocalTime departureTime;
   private String fromAirport;
   private String toAirport;
   private int airplaneId;
   private int menuId;
   private Seat[][] economySeat;
   private Seat[][] businessSeat;


   /*
    * Constructor
    */


   public Flight(int id, String fromAirport, String toAirport, int airplaneId, int menuId, Seat[][] economySeat, Seat[][] businessSeat, LocalDate departureDate, LocalTime departureTime){

      setId(id);
      setFromAirport(fromAirport);
      setToAirport(toAirport);
      setAirplaneId(airplaneId);
      setMenuId(menuId);
      setEconomySeat(economySeat);
      setBusinessSeat(businessSeat);
      setDepartureDate(departureDate);
      setDepartureTime(departureTime);

   }   // End of constructor


   /*
    * Setters
    */


   public void setId(int id){
      this.id = id;
   }   // id Setter

   public void setFromAirport(String Airport){
      this.fromAirport = fromAirport;
   }   // fromAirport Setter

   public void setToAirport(String toAirport){
      this.toAirport = toAirport;
   }   // toAirport Setter

   public void setAirplaneId(int airplaneId){
      this.airplaneId = airplaneId;
   }   // airplaneId Setter

   public void setMenuId(int menuId){
      this.menuId = menuId;
   }   // menuId Setter

   public void setEconomySeat(Seat[][] economySeat){
      this.economySeat = economySeat;
   }   // seat Setter

   public void setBusinessSeat(Seat[][] businessSeat){
      this.businessSeat = businessSeat;
   }   // seat Setter
   
   public void setDepartureDate (LocalDate departureDate){
	   this.departureDate = departureDate;
   }   // departureDate Setter

   public void setDepartureTime (LocalTime departureTime){
	   this.departureTime = departureTime;
   }   // departureTime Setter
   
   
   /*
    * Getters
    */


   public int getId(){
      return this.id;
   }   // id Getter

   public int getAirplaneId(){
      return this.airplaneId;
   }   // airplaneid Getter

   public Seat[][] getEconomySeat(){
      return this.economySeat;
   }   // seat Getter

   public Seat[][] getBusinessSeat(){
      return this.businessSeat;
   }   // seat Getter

   public int getMenuId(){
      return this.menuId;
   }   // menuId Getter

   public String getFromAirport(){
      return this.fromAirport;
   }   // fromAirport Getter

   public String getToAirport(){
      return this.toAirport;
   }   // toAirport Getter

   public LocalDate getDepartureDate(){
	   return this.departureDate;
   }   // departureDate Getter

   public LocalTime getDepartureTime(){
	   return this.departureTime;
   }   // departureTime Getter
   
   
}   // End of class

